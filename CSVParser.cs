﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tiershop24Import.model;

namespace Tiershop24Import
{
    class CSVParser
    {
        public List<ShopItem> parseCSVFile(String url)
        {
            CSVLoader loader = new CSVLoader();
            String data = loader.getCSV(url);
            StringReader sr = new StringReader(data);
            try
            {


                using (TextFieldParser csvParser = new TextFieldParser(sr))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { ";" });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    // Skip the row with the column names
                    csvParser.ReadLine();

                    List<ShopItem> shopItems = new List<ShopItem>();

                    while (!csvParser.EndOfData)
                    {
                        // Read current line fields, pointer moves to the next line.
                        string[] fields = csvParser.ReadFields();
                        ShopItem shopItem = new ShopItem(fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6], fields[7], fields[8], fields[9], fields[10], fields[11], fields[12], fields[13], fields[14], fields[15], fields[16], fields[17], fields[18], fields[19], fields[20], fields[21], fields[22], fields[23]);
                        shopItems.Add(shopItem);
                    }

                    Console.WriteLine("Items Found: " + shopItems.Count);
                    return shopItems;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured while parsing the csv document");
                return null;
            }
        }
    }
}