﻿using System;
using System.Collections.Generic;
using Tiershop24Import.model;

namespace Tiershop24Import
{
    class Program
    {
        static String url = "http://www.zoodrop.de/csv/download/zoodrop_utf8.csv";

        static void Main(string[] args)
        {
            CSVParser parser = new CSVParser();
            List<ShopItem> shopItems = parser.parseCSVFile(url);
        }
    }

}

