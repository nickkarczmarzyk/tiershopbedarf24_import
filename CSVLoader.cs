﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Tiershop24Import
{
    class CSVLoader
    {
        public string getCSV(String url)
        {
            Console.WriteLine("Beginning to get csv File");
            try
            {
                // Init of Request
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                // Performing Request
                using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                {
                    string results = sr.ReadToEnd();
                    sr.Close();
                    Console.WriteLine("Sucessfully downloaded csv file");
                    return results;
                }
            } catch (Exception e)
            {
                // missing exception handling
                Console.WriteLine("An error occured while downloading the csv file");
                Console.WriteLine(e);
            }
            return null;
        }
    }
}
