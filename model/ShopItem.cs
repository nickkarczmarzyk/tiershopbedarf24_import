﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiershop24Import.model
{
    class ShopItem
    { 
        public String EAN { get; set; }
        public String artikelnummer { get; set; }
        public String verkaufsstatus { get; set; }
        public String lagerstatus { get; set; }
        public String bestand { get; set; }
        public String hersteller { get; set; }
        public String titel { get; set; }
        public String verkaufspreis { get; set; }
        public String uvp { get; set; }
        public String grundpreis { get; set; }
        public String grundpreis_menge { get; set; }
        public String grundpreis_einheit { get; set; }
        public String verpackungseinheit { get; set; }
        public String mwst { get; set; }
        public String kategorie { get; set; }
        public String beschreibung { get; set; }
        public String bild1 { get; set; }
        public String bild2 { get; set; }
        public String bild3 { get; set; }
        public String bild4 { get; set; }
        public String bild5 { get; set; }
        public String bild6 { get; set; }
        public String bild7 { get; set; }
        public String bild8 { get; set; }

        public ShopItem(string eAN, string artikelnummer, string verkaufsstatus, string lagerstatus, string bestand, string hersteller, string titel, string verkaufspreis, string uvp, string grundpreis, string grundpreis_menge, string grundpreis_einheit, string verpackungseinheit, string mwst, string kategorie, string beschreibung, string bild1, string bild2, string bild3, string bild4, string bild5, string bild6, string bild7, string bild8)
        {
            EAN = eAN;
            this.artikelnummer = artikelnummer;
            this.verkaufsstatus = verkaufsstatus;
            this.lagerstatus = lagerstatus;
            this.bestand = bestand;
            this.hersteller = hersteller;
            this.titel = titel;
            this.verkaufspreis = verkaufspreis;
            this.uvp = uvp;
            this.grundpreis = grundpreis;
            this.grundpreis_menge = grundpreis_menge;
            this.grundpreis_einheit = grundpreis_einheit;
            this.verpackungseinheit = verpackungseinheit;
            this.mwst = mwst;
            this.kategorie = kategorie;
            this.beschreibung = beschreibung;
            this.bild1 = bild1;
            this.bild2 = bild2;
            this.bild3 = bild3;
            this.bild4 = bild4;
            this.bild5 = bild5;
            this.bild6 = bild6;
            this.bild7 = bild7;
            this.bild8 = bild8;
        }
    }
}
